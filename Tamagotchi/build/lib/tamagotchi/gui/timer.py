import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class StackedWidget(QStackedWidget):

    def __init__(self, parent=None):
        QStackedWidget.__init__(self, parent=parent)
        self.stoptimer = QPushButton("stop",self)
        self.stoptimer.move(50,75)
        self.stoptimer.clicked.connect(self.stater)
        self.timelabel = QLabel(self)
        self.timelabel.move(70,35)
        self.startit()
        self.gettim = []

    def startit(self):
        self.timer = QTimer()
        self.time = QTime(0, 0, 0)
        self.timer.timeout.connect(self.timerEvent)
        self.timer.start(100)    
    def timerEvent(self):
        self.time = self.time.addSecs(1)
        self.timetolabel = self.time.toString("hh:mm:ss")
        self.timelabel.setText(self.timetolabel)
    
    def stater(self):
        self.timer.stop()
        self.Usertimesave = self.time.toString("hh:mm:ss")
        self.gettim.append(self.Usertimesave)

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent=parent)
        super(MainWindow, self).__init__()
        self.setMaximumSize(200,200)
        self.setGeometry(50,50,200,200)
        self.StackName = StackedWidget()
        self.setCentralWidget(self.StackName)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())