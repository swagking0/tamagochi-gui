from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import subprocess
import time
import sys
import csv
import collections

class StackedWidget(QStackedWidget):

    def __init__(self, parent=None):
        QStackedWidget.__init__(self, parent=parent)
        self.home()

    def home(self):
        #font_parameters
        font_label = QFont("Times",25,QFont.Bold)
        font_sub_label = QFont("Times",12,QFont.Bold)
        #size_parameters
        btn_Size = QSize(200,40)
        Lab_Size = QSize(150,80)
        Lab_sub_Size = QSize(150,80)
        Edit_Size = QSize(350,35)

        #Widgets
        btnNext = QPushButton("Next", self)
        self.User_name = QLineEdit(self)
        self.User_name.setFont(font_sub_label)
        self.User_Reg = QLineEdit(self)
        self.User_Reg.setFont(font_sub_label)
        Label_Username = QLabel(self)
        Label_Username.setText("@UserName")
        Label_Username.setFont(font_sub_label)
        Label_Username.setStyleSheet('QLabel {color:#1e272e}') #https://flatuicolors.com
        Label_Userreg = QLabel(self)
        Label_Userreg.setText("@UserReg")
        Label_Userreg.setFont(font_sub_label)
        Label_Userreg.setStyleSheet('QLabel {color:#1e272e}') #https://flatuicolors.com

        #Widgets sizes
        btnNext.resize(btn_Size)
        Label_Username.resize(Lab_sub_Size)
        Label_Userreg.resize(Lab_sub_Size)
        self.User_name.resize(Edit_Size)
        self.User_Reg.resize(Edit_Size)

        #Widgets positions
        btnNext.move(270,330)
        self.User_name.move(120,195)
        self.User_Reg.move(120,265)
        Label_Username.move(115,140)
        Label_Userreg.move(115,210)

        #line-edit checking schema
        self.User_Reg.setValidator(QIntValidator())
        self.User_Reg.setMaxLength(7)

        #This area is used to define signals of the QButtons
        btnNext.clicked.connect(self.onNext)
    
    def paintEvent(self, event):
        painter = QPainter(self)
        painter.drawPixmap(self.rect(), QPixmap("resources/GUI-StartPage.png"))
        QStackedWidget.paintEvent(self, event)

    #This area is used to define functions of the QButtons
    def onNext(self):
        self.User_name_data = self.User_name.text()
        self.User_Reg_data = self.User_Reg.text()
        UserdataStore = [self.User_name_data,self.User_Reg_data]
        self.UserdataFile = 'resources/userdata.csv'

        with open(self.UserdataFile, 'r') as filein:
            old_data = list(csv.reader(filein))
            new_data = old_data
            new_data.append(UserdataStore)
        with open(self.UserdataFile, 'w') as fileout:
            writer = csv.writer(fileout)
            writer.writerows(old_data)
            
        time.sleep(1)
        process = subprocess.Popen(["tamagotchi"], cwd="Tamagotchi/")
        process_time = subprocess.Popen(['python', 'timer.py'])
        time.sleep(0.5)
        sys.exit(self)

        
class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent=parent)
        super(MainWindow, self).__init__()
        self.setGeometry(50,50,613,626)
        self.setMaximumSize(613,626)
        self.setWindowTitle("Tamagochi")
        self.setCentralWidget(StackedWidget())

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())